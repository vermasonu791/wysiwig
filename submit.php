<html>
<head>
<title>Wysiwyg editor</title>
 <!-- Meta tags -->
<meta charset="utf-8">
<!--Title icon-->
<link rel="icon" href="logo.png">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <!-- Bootstrap CSS and JavaScript-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"> 
<!--External css-->
<link rel="stylesheet" href="style.css">    
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>    
</head>
<body>
<?php
//adding header to page
include 'header.php';
?>     
<div class="container">
  <br><br>
  <div class="col-md-12" id="view_post">
  <?php 
  echo $_POST['textarea'];
  ?>
</div>    
<br>
<br>
<br>
<br>
<?php
//adding footer on the view page
include 'footer.php';
?>
</div> 
</body>
</html>