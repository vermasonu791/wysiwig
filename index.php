<html>
<head>
<title>Wysiwyg editor</title>
 <!-- Meta tags -->
<meta charset="utf-8">
<link rel="icon" href="logo.png">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <!-- Bootstrap CSS and JavaScript-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">    
<!-- External css and javascript-->
<link rel="stylesheet" href="style.css">   
<script src="app.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>     
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>    
</head>
<!--on page load this two function will invoked one is used to make iframe editable-->
<body onLoad="iframe();function1();">
<script>
      //This logic is totally for drag and drop image
    $(document).ready(function(){
        $('.files_drag_area').on('dragover',function(event){
            //method stop the default action
            event.preventDefault();  
            event.stopPropagation();
            //when you drag image over the box you will see some css
            $(this).addClass('file_drag_over');        
        });
        
        $('.files_drag_area').on('dragleave',function(event){
            event.preventDefault();  
            event.stopPropagation();
            //when you drag image over the box you will see some css
            $(this).removeClass('file_drag_over');
            
        });
        //After drop image on the box
        $('.files_drag_area').on('drop',function(event) {	
            event.preventDefault();  
            event.stopPropagation();
            //Removing css by selected class
            $(this).removeClass('file_drag_over'); 
            /*formdata is object which collect the data in the 
            form key/pair value and you can send the through http
            request
            */
           var formData= new FormData();
            var file_list = event.originalEvent.dataTransfer.files;
            console.log(file_list);
            //if you Droping one or more images 
            for(var i=0; i<file_list.length;i++)
            { 
                //append formdata
                formData.append('file[]',file_list[i]);
            } 
            console.log(formData);
            //ajax reuest to pass the formdata
            $.ajax({
                url:"upload.php",
                method:"POST",
                data:formData,
                contentType:false,
                cache:false,
                processData:false,
                success:function(data){
                    //get some data back return by php file.
                    $('#uploaded_file').html(data);
                }
            })
        }); 
    });
    
    function function1() {
        var x = document.getElementById("editor");
        var y = (x.contentWindow || x.contentDocument);
        if (y.document)y = y.document;
        y.body.innerHTML = "<div></div>";
    }
    
    function function2() {
        var iframe = document.getElementById("editor");
        var elmnt = iframe.contentWindow.document.getElementsByTagName("div")[0];
        elmnt.innerHTML = document.getElementById("uploaded_file").innerHTML;
        }
</script> 
<!--Add header to the-->
<?php
include 'header.php';
?>
<!--page container-->
<div class="container">
    <br><br>
    <form action="submit.php" method="post" id="rtf">
        <!--Adding buttons -->
        <button type="button" onclick="bold();"><i class="fa fa-bold" aria-hidden="true"></i></button>
        <button type="button" onclick="italic();"><i class="fa fa-italic" aria-hidden="true"></i></button>  
        <button type="button" onclick="underline();"><i class="fa fa-underline" aria-hidden="true"></i></button>  
        <button type="button" onclick="link();"><i class="fas fa-link"></i></button>  
        <button type="button" onclick="showdrag();"><i class="far fa-image"></i></button>
        <hr>
        <!--Image Drop box it default hide when you click on the image button then it will show -->
        <div class="files_drag_area" id="drag" onmouseout="function2();">Drop image here to upload</div>   
        <br>
        <!--div tag where you get back image from the server-->
        <div id="uploaded_file" style="display:none;"></div>
        <textarea name="textarea" id="textarea" cols="30" rows="10" style="display:none;"></textarea>  
        <iframe id="editor" name="editor" style="width:100%; height:400;" ></iframe>
        <br><br>
        <!--submit button for viewing the post-->
        <input type="button" value="View post" id="post_button" onclick="formsubmit();">
    </form>
    <br><br><br>
    <!--Add footer the page-->
    <?php
    include 'footer.php';
    ?>
</div>      
</body>
</html>   